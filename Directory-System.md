rectory System Template   
I want to keep my directory hierarchy clean and persistent. I didn't
found any Template, so I've decided to create my own. If you know how
something like that is called, please contact me!  

***
###Markdown Syntax
    ###Folder0
    ####Folder1 = "~/Folder1"
    * Folder2 in ####Folder1 (~/Folder1)
        * in Folder3
    `~/` Symbolic link(e.g. ln -s) to Folder0
    `General Comment`
####File-tree view

    ./Folder0
    ./Folder0/Folder1
    ./Folder0/Folder1/Folder2
    ./Folder0/Folder1/Folder2/Folder3
    ./Folder0/Folder1/Folder0

***
###Files
####Dynamic
* Projects
    * _old `unrecent projects`
* Edits
    * _old `unrecent projects`
        * Year
            * Project-Name
* Temp

####Static

* Sound
    * Podcasts
    * Music
* Videos
    * Music-Videos
    * Films
    * Documentations
    * Screencastings
        * _old
    * Talks
        * `~/Dynamic/Documents/Slides/`
* Documents
    * Software
    * Papers
    * Articles
    * Slides
    * PersonalArticles
    * Logs
        * Chatlogs
        * Siterips `Year/Month/Day`
        * Screenshots `Year/Month/Day`
            * `~/Static/Videos/Screencastings/`
* Pictures 
    * Saved
        * Animated
        * High-Res
        * Mid-Res
        * Low-Res
    * Photos `Year/Month/Day`
        * _old `not current year`
    * `~/Dynamic/Edits/`
* Generic `Year/Month/Day`
    * _old `not current year`
* Others Files

***

###Missing Handling for:
* Random but personal Photo from the internet
* School-Stuff
    * Notes
###Todo
* Management for Chatlogs
* Documents Order
* VirtualBox

***
###Old Directory System

    Foldertree:
    
    1. images
    2. documents
    3. dev
    4. video
    5. audio
    ---
    
    Work In Progress
    Archiv(Finished-Files)
    
    ---
    
    Manual;Type;Year;Month;var(project)
    Research;Type;var(papers)
    Work;Type;Varg
